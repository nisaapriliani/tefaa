package com.example.tefa.ui.onboarding

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.example.tefa.R
import com.example.tefa.base.BaseActivity
import com.example.tefa.databinding.ActivityOnBoardingBinding
import com.example.tefa.ui.MainActivity
import com.example.tefa.utils.IntentKey

class OnBoardingActivity : BaseActivity() {
    private lateinit var binding: ActivityOnBoardingBinding
    private var dotsCount: Int = 0
    private lateinit var dots: Array<ImageView?>
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    private var avatar: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnBoardingBinding.inflate(layoutInflater)
        initIntent()
        initView()
        initAction()
        setContentView(binding.root)
    }

    private fun initIntent() {
        avatar = intent.getStringExtra(IntentKey.KEY_USER).toString()
    }

    private fun initView() {
        viewPagerAdapter = ViewPagerAdapter(this)
        binding.vpOnBoarding.adapter = viewPagerAdapter
        dotsCount = viewPagerAdapter.count
        dots = arrayOfNulls(dotsCount)
        for (i in 0 until dotsCount) {
            dots[i] = ImageView(this)
            dots[i]?.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.indicator_dots_notactive
                )
            )
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(8, 0, 8, 0)
            binding.sliderDotsPanel.addView(dots[i], params)
        }
        dots[0]?.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.indicator_dots_active))
        binding.vpOnBoarding.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                if (position == dotsCount-1) {
                    binding.btnAction.text = getString(R.string.start)
                } else {
                    binding.btnAction.text = getString(R.string.skip)
                }
            }

            override fun onPageSelected(position: Int) {

                for (i in 0 until dotsCount) {
                    dots[i]?.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@OnBoardingActivity,
                            R.drawable.indicator_dots_notactive
                        )
                    )
                }
                dots[position]?.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@OnBoardingActivity,
                        R.drawable.indicator_dots_active
                    )
                )
                binding.vpOnBoarding.adapter?.notifyDataSetChanged()
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    private fun initAction() {
        binding.btnAction.setOnClickListener {
            // Aditya Eka Santoso
            pref?.isFirstOpen = false
            val intent = Intent(this@OnBoardingActivity, MainActivity::class.java)
            intent.putExtra(IntentKey.KEY_USER, avatar)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }
}