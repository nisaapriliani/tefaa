package com.example.tefa.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.example.tefa.R

object LoadingUtils {
    private var builder: AlertDialog.Builder? = null
    private var loading: AlertDialog? = null

    @SuppressLint("InflateParams")
    fun showLoading(context: Context) {
        if (builder == null && loading == null) {
            val view = LayoutInflater.from(context).inflate(R.layout.custom_loading, null)
            builder = AlertDialog.Builder(context)
            builder?.setView(view)
            builder?.setCancelable(false)
            loading = builder?.create()
            loading?.show()
            val displayMetrics = DisplayMetrics()
            val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowManager.defaultDisplay.getMetrics(displayMetrics)
            val displayWidth = displayMetrics.widthPixels
            val dialogWindowWidth = (displayWidth * 0.35f).toInt()
            loading?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            loading?.window?.setLayout(dialogWindowWidth, dialogWindowWidth)
        }
    }

    fun hideLoading() {
        if (loading != null && loading?.isShowing == true) {
            loading = try {
                loading?.dismiss()
                null
            } catch (e: Exception) {
                null
            }
        }
        builder = null
    }

    fun isShowLoading() : Boolean{
        return loading?.isShowing?: false
    }
}