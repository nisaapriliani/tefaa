package com.example.tefa.utils

import kotlin.math.roundToInt

object MathUtils {
    fun add(a: Int, b: Int) : Int {
        return a+b
    }

    fun multiple(a: Int, b: Int) : Int {
        return a*b
    }

    fun idealWeight(height: Int): Int {
        return ((height - 100) - ((height-100) * 0.1)).roundToInt()
    }
}