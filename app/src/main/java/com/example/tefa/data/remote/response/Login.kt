package com.example.tefa.data.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

object Login {
//        "email": "eve.holt@reqres.in",
//        "password": "cityslicka"

    // Regita Hutari
    data class Request(
        val email: String,
        val password: String
    )

    // Aditya Eka
    @Parcelize
    data class Response(
        @SerializedName("token")
        val token: String? = null
    ) : Parcelable
}