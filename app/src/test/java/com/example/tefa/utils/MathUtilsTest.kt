package com.example.tefa.utils

import org.junit.Assert.assertEquals
import org.junit.Test


internal class MathUtilsTest {

    @Test
    fun `when call function add() should return a + b`() {
        val result: Int = MathUtils.add(3,5)
        assertEquals(8, result)
    }

    @Test
    fun `when call function multiple() should return a x b`() {
        val result: Int = MathUtils.multiple(3,2)
        assertEquals(6, result)
    }

    @Test
    fun `when call function idealWeight() should return BMI = (height-100)-(10 percent of (height-100))`() {
        var result: Int = MathUtils.idealWeight(175)
        assertEquals(68, result)

        result = MathUtils.idealWeight(165)
        assertEquals(59, result)

        result = MathUtils.idealWeight(178)
        assertEquals(70, result)
    }
}